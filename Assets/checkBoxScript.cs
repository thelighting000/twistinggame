﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class checkBoxScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
        Destroy(this.gameObject, 0.1f);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "spiningcenter")
        {
            collision.GetComponent<spinnerLock>().locked -= 1;
        }
    }
}
