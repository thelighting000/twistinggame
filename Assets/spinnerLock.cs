﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spinnerLock : MonoBehaviour {
    public int locked;
    public bool isLocked;

	// Use this for initialization
	void Start () {
        //locked = 0;
	}
	
	// Update is called once per frame
	void Update () {
        if (locked > 0)
        {
            isLocked = true;
            gameObject.transform.localScale = new Vector3(1, 1, 1);
            gameObject.GetComponent<SpriteRenderer>().color = Color.blue;
        }
        else
        {
            isLocked = false;
            gameObject.transform.localScale = new Vector3(2.5f, 2.5f, 1);
            gameObject.GetComponent<SpriteRenderer>().color = Color.white;
        }
	}
}
