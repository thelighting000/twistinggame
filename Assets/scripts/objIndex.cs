﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class objIndex : MonoBehaviour {
    public int index;
    public int x;
    public int y;
    public bool speDead, horiDead, vertiDead;
    public Vector2 arrayindex;

    public float speed;
    float startTime;
    Vector2 transA;
    Vector2 A, B;
    GameObject ObjA;

    private void Start()
    {
        speDead = false;
        horiDead = false;
        vertiDead = false;
    }
}
