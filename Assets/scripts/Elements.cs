﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Elements : MonoBehaviour {
    // prefabs and public game objects
    public GameObject spinPref;
    public GameObject[] spritePrefs;
    public GameObject location;
    public GameObject[,] locations;
    public Text bonustxt;
    public int Xmax, Ymax;
    public int score = 0;
    public Transform locationBox, elementBox, spinnerBox;
    public GameObject checkBox;

    public enum State
    {
        Start,
        Standby,
        Check,
        Animate,
        Destroy,
        CreateSpecials,
        CreateNormal,
        Move,
        Click
    }

    public State state;

    public bool animating;
    bool horiInstantiate, vertiInstantiate, crossinstantiate;
    // arrays
    int[,] objArrayIndex;
    GameObject[,] objArray;
    
    int tempV, tempH;
    int v, h, itemCount, multiplier;

    float exitTime, moveExit;

    Vector2 tempT_A, tempT_B, tempT_C, tempT_D;
    GameObject A, B, C, D;

    // Use this for initialization
    void Start () {
        state = State.Start;

        // set up spinners
        for (int y = 0; y < Ymax-1; y++)
        {
            for (int x = 0; x < Xmax-1; x++)
            {
                GameObject tempObj = Instantiate(spinPref, new Vector2(x + 0.5f - Xmax / 2, -y - 0.5f + Ymax / 2), Quaternion.identity);
                tempObj.AddComponent<objIndex>();
                tempObj.transform.parent = spinnerBox;
                int tempRand = Random.Range(0, 4);
                if (tempRand == 1)
                {
                    tempObj.GetComponent<spinnerLock>().locked = 1;
                }
                tempObj.GetComponent<objIndex>().x = x;
                tempObj.GetComponent<objIndex>().y = y;
            }
        }

        objArrayIndex = new int[Xmax+1, Ymax+1];
        objArray = new GameObject[Xmax, Ymax];
        locations = new GameObject[Xmax, Ymax];

        itemCount = 0;
        multiplier = 1;

        animating = false;
        horiInstantiate = true;
        vertiInstantiate = true;
        crossinstantiate = true;
    }

	// Update is called once per frame
	void Update () {
        switch (state)
        {
            case State.Start:
                CreateBoard();
                break;
            case State.Check:
                elemsCheck();
                break;
            case State.Destroy:
                elemsDest();
                break;
            case State.Animate:
                setAnimate();
                break;
            case State.CreateSpecials:
                elemsCreate();
                break;
            case State.Click:
                break;
            case State.Standby:
                standby();
                break;
            case State.CreateNormal:
                createNorm();
                break;
            case State.Move:
                moveObjects();
                break;
            default:
                break;
        }
        multiplierCal();
    }

    void setIndex()
    {
        for (int j = 0; j < Ymax; j++)
        {
            for (int i = 0; i < Xmax; i++)
            {
                objArray[i, j].GetComponent<objIndex>().arrayindex = new Vector2(i, j);
            }
        }
    }

    void standby()
    {
        if (Time.time >= exitTime)
        {
            state = State.CreateSpecials;
        }
        moveExit = Time.time + 0.75f;
    }

    void CreateBoard()
    {
        //create array of numbers and game objects.
        for (int y = 0; y > -Ymax; y--)
        {
            for (int x = 0; x < Xmax; x++)
            {
                int objIndex = Random.Range(0, 4);
                GameObject tempObj = Instantiate(spritePrefs[objIndex], new Vector2(x - Xmax / 2, y + Ymax / 2), Quaternion.identity);
                objArrayIndex[x, -y] = tempObj.GetComponent<objIndex>().index;
                objArray[x, -y] = tempObj;
                tempObj.transform.parent = elementBox;
                tempObj.GetComponent<objIndex>().x = x;
                tempObj.GetComponent<objIndex>().y = -y;

                GameObject tempLo = Instantiate(location, new Vector2(x - Xmax / 2, y + Ymax / 2), Quaternion.identity);
                locations[x, -y] = tempLo;
                tempLo.transform.parent = locationBox;
                tempLo.GetComponent<objIndex>().x = x;
                tempLo.GetComponent<objIndex>().y = -y;
            }
        }
        state = State.Check;
    }
    //move objects on click, forward.
    public void updatePosition(int x, int y)
    {
        multiplier = 1;
        itemCount = 0;
        int tempx = x;
        int tempy = y;
        int[,] temppos = new int[1, 1];
        GameObject[,] tempobjarr = new GameObject[1, 1];

        temppos[0, 0] = objArrayIndex[tempx, tempy];
        objArrayIndex[tempx, tempy] = objArrayIndex[tempx, tempy + 1];
        objArrayIndex[tempx, tempy + 1] = objArrayIndex[tempx + 1, tempy + 1];
        objArrayIndex[tempx + 1, tempy + 1] = objArrayIndex[tempx + 1, tempy];
        objArrayIndex[tempx + 1, tempy] = temppos[0, 0];

        tempobjarr[0, 0] = objArray[tempx, tempy];
        objArray[tempx, tempy] = objArray[tempx, tempy + 1];
        objArray[tempx, tempy + 1] = objArray[tempx + 1, tempy + 1];
        objArray[tempx + 1, tempy + 1] = objArray[tempx + 1, tempy];
        objArray[tempx + 1, tempy] = tempobjarr[0, 0];

        setIndex();
        state = State.Check;
    }
    //move objects on click, backward.
    public void reversePosition(int x, int y)
    {
        multiplier = 1;
        itemCount = 0;
        int tempx = x;
        int tempy = y;
        int[,] temppos = new int[1, 1];
        GameObject[,] tempobjarr = new GameObject[1, 1];

        temppos[0, 0] = objArrayIndex[tempx, tempy];
        objArrayIndex[tempx, tempy] = objArrayIndex[tempx + 1, tempy];
        objArrayIndex[tempx + 1, tempy] = objArrayIndex[tempx + 1, tempy + 1];
        objArrayIndex[tempx + 1, tempy + 1] = objArrayIndex[tempx, tempy + 1];
        objArrayIndex[tempx, tempy + 1] = temppos[0, 0];

        tempobjarr[0, 0] = objArray[tempx, tempy];
        objArray[tempx, tempy] = objArray[tempx + 1, tempy];
        objArray[tempx + 1, tempy] = objArray[tempx + 1, tempy + 1];
        objArray[tempx + 1, tempy + 1] = objArray[tempx, tempy + 1];
        objArray[tempx, tempy + 1] = tempobjarr[0, 0];

        setIndex();
        state = State.Check;
    }
    
    //things to do after clicked special element block.
    public void speElemClick(int x, int y)
    {
        animating = true;
        multiplier = 2;
        itemCount = 0;
        for (int j = y-1; j <= y+1; j++)
        {
            for (int i = x - 1; i <= x + 1; i++)
            {
                if (i >= 0 && i < Xmax && j >= 0 && j < Ymax)
                {
                    if (objArray[i, j].GetComponent<objIndex>().index == 10 && i !=x && j !=y)
                    {
                        objArray[i, j].GetComponent<objIndex>().speDead = true;
                    }
                    if (objArray[i, j].GetComponent<objIndex>().index == 11)
                    {
                        objArray[i, j].GetComponent<objIndex>().horiDead = true;
                    }
                    if (objArray[i, j].GetComponent<objIndex>().index == 12)
                    {
                        objArray[i, j].GetComponent<objIndex>().vertiDead = true;
                    }
                    else
                    {
                        objArray[i, j].GetComponent<Animator>().SetBool("dead", true);
                        sendObjective(objArrayIndex[i, j]);
                        Destroy(objArray[i, j], 1.5f);
                        objArrayIndex[i, j] = 99;
                    }
                }
            }
        }
        state = State.CreateNormal;
    }

    public void horiClick(int x, int y)
    {
        animating = true;
        multiplier = 1;
        itemCount = 0;
        for (int i = 0; i < Xmax; i++)
        {
            if (objArray[i, y].GetComponent<objIndex>().index == 10)
            {
                objArray[i, y].GetComponent<objIndex>().speDead = true;
            }
            if (objArray[i, y].GetComponent<objIndex>().index == 12)
            {
                objArray[i, y].GetComponent<objIndex>().vertiDead = true;
            }
            objArray[i, y].GetComponent<Animator>().SetBool("dead", true);
            sendObjective(objArrayIndex[i, y]);
            Destroy(objArray[i, y], 1.5f);
            objArrayIndex[i, y] = 99;
        }
        state = State.CreateNormal;
    }

    public void vertiClick(int x, int y)
    {
        animating = true;
        multiplier = 1;
        itemCount = 0;
        for (int i = 0; i < Ymax; i++)
        {
            if (objArray[x, i].GetComponent<objIndex>().index == 10)
            {
                objArray[x, i].GetComponent<objIndex>().speDead = true;
            }
            if (objArray[x, i].GetComponent<objIndex>().index == 11)
            {
                objArray[x, i].GetComponent<objIndex>().horiDead = true;
            }
            objArray[x, i].GetComponent<Animator>().SetBool("dead", true);
            sendObjective(objArrayIndex[x, i]);
            Destroy(objArray[x, i], 1.5f);
            objArrayIndex[x, i] = 99;
        }
        state = State.CreateNormal;
    }

    public void elemsCheck()
    {
        if (!animating)
        {
            for (int y = 0; y < Ymax; y++)
            {
                for (int x = 0; x < Xmax; x++)
                {
                    h = 1;
                    v = 1;
                    int temp = objArray[x, y].GetComponent<objIndex>().index;
                    checkMatchH(temp, x, y);
                    checkMatchV(temp, x, y);
                    indexCheck(temp, x, y);
                }
            }
        }
        state = State.Animate;
    }
    //send these objects and transform to lerp script.
    public void objCheck(int x, int y)
    {
        A = objArray[x, y];
        B = objArray[x + 1, y];
        C = objArray[x + 1, y + 1];
        D = objArray[x, y + 1];

        tempT_A = locations[x, y].transform.position;
        tempT_B = locations[x + 1, y].transform.position;
        tempT_C = locations[x + 1, y + 1].transform.position;
        tempT_D = locations[x, y + 1].transform.position;

        GetComponent<objLerp>().sendTransA(A.transform.position, tempT_A, A);
        GetComponent<objLerp>().sendTransB(B.transform.position, tempT_B, B);
        GetComponent<objLerp>().sendTransC(C.transform.position, tempT_C, C);
        GetComponent<objLerp>().sendTransD(D.transform.position, tempT_D, D);
    }

    //recreate elements after destroy.
    void elemsCreate()
    {
        animating = false;
        for (int y = 0; y < Ymax; y++)
        {
            for (int x = 0; x < Xmax; x++)
            {
                if (objArrayIndex[x, y] == 100 && objArray[x, y] == null)
                {
                    GameObject tempObj = Instantiate(spritePrefs[4], locations[x, y].transform.position, Quaternion.identity);
                    objArrayIndex[x, y] = tempObj.GetComponent<objIndex>().index;
                    objArray[x, y] = tempObj;
                    tempObj.transform.parent = elementBox;
                    tempObj.GetComponent<objIndex>().x = x;
                    tempObj.GetComponent<objIndex>().y = y;
                    score += 125*multiplier;
                }
                else if (objArrayIndex[x, y] == 101 && objArray[x, y] == null)
                {
                    GameObject tempObj = Instantiate(spritePrefs[5], locations[x, y].transform.position, Quaternion.identity);
                    objArrayIndex[x, y] = tempObj.GetComponent<objIndex>().index;
                    objArray[x, y] = tempObj;
                    tempObj.transform.parent = elementBox;
                    tempObj.GetComponent<objIndex>().x = x;
                    tempObj.GetComponent<objIndex>().y = y;
                    score += 75 * multiplier;
                }
                else if (objArrayIndex[x, y] == 102 && objArray[x, y] == null)
                {
                    GameObject tempObj = Instantiate(spritePrefs[6], locations[x, y].transform.position, Quaternion.identity);
                    objArrayIndex[x, y] = tempObj.GetComponent<objIndex>().index;
                    objArray[x, y] = tempObj;
                    tempObj.transform.parent = elementBox;
                    tempObj.GetComponent<objIndex>().x = x;
                    tempObj.GetComponent<objIndex>().y = y;
                    score += 75 * multiplier;
                }
            }
        }
        state = State.Move;
    }

    void createNorm()
    {
        animating = false;
        for (int y = 0; y < Ymax; y++)
        {
            for (int x = 0; x < Xmax; x++)
            {
                locations[x, y].GetComponent<moveObjs>().enabled = false;
                if (objArrayIndex[x, y] == 99 && objArray[x, y] == null)
                {
                    int objIndex = Random.Range(0, 4);
                    GameObject tempObj = Instantiate(spritePrefs[objIndex], locations[x, y].transform.position, Quaternion.identity);
                    objArrayIndex[x, y] = tempObj.GetComponent<objIndex>().index;
                    objArray[x, y] = tempObj;
                    tempObj.transform.parent = elementBox;
                    tempObj.GetComponent<objIndex>().x = x;
                    tempObj.GetComponent<objIndex>().y = y;
                    score += 25 * multiplier;
                }
            }
        }
        setIndex();
        state = State.Check;
    }

    void elemsDest()
    {
        for (int j = 0; j < Ymax; j++)
        {
            for (int i = 0; i < Xmax; i++)
            {
                if (objArrayIndex[i, j] >= 99)
                {
                    Destroy(objArray[i, j], 1.5f);
                    Instantiate(checkBox, locations[i, j].transform.position, Quaternion.identity);
                }
            }
        }
        exitTime = Time.time + 1.75f;
        state = State.Standby;
    }

    void indexCheck(int temp, int x, int y)
    {
        if (h > 2)
        {
            changeLineIndex(temp, x, y);
            if (h > 3 && objArrayIndex[x + 1, y] != temp && horiInstantiate)
            {
                changeLineIndexHS(temp, x, y);
            }
        }
        if (v > 2)
        {
            changeLineIndex(temp, x, y);
            if (v > 3 && objArrayIndex[x, y + 1] != temp && vertiInstantiate)
            {
                changeLineIndexVS(temp, x, y);
            }
            if (h > 2 && crossinstantiate)
            {
                changeBothLineIndex(temp, x, y);
            }
        }
    }

    void checkMatchV(int temp, int x, int y)
    {
        vertiInstantiate = true;
        int UP = y - 1;
        while (UP >= 0)
        {
            if (temp == objArray[x, UP].GetComponent<objIndex>().index && temp < 10)
            {
                v++;
                if (100 == objArrayIndex[x, UP] || 101 == objArrayIndex[x, UP] || 102 == objArrayIndex[x, UP])
                {
                    vertiInstantiate = false;
                }
                if (100 == objArrayIndex[x, UP])
                {
                    crossinstantiate = false;
                }
            }
            else
            {
                break;
            }
            UP--;
        }
        int DOWN = y + 1;
        while (DOWN < Ymax)
        {
            if (temp == objArray[x, DOWN].GetComponent<objIndex>().index && temp < 10)
            {
                v++;
                if (100 == objArrayIndex[x, DOWN] || 101 == objArrayIndex[x, DOWN] || 102 == objArrayIndex[x, DOWN])
                {
                    vertiInstantiate = false;
                }
                if (100 == objArrayIndex[x, DOWN])
                {
                    crossinstantiate = false;
                }
            }
            else
            {
                break;
            }
            DOWN++;
        }
    }

    void checkMatchH(int temp, int x, int y)
    {
        horiInstantiate = true;
        crossinstantiate = true;
        int LEFT = x - 1;
        while (LEFT >= 0)
        {
            if (temp == objArray[LEFT, y].GetComponent<objIndex>().index && temp < 10)
            {
                h++;
                if (objArrayIndex[LEFT, y] == 100 || objArrayIndex[LEFT, y] == 101 || objArrayIndex[LEFT, y] == 102)
                {
                    horiInstantiate = false;
                }
                if (objArrayIndex[LEFT, y] == 100)
                {
                    crossinstantiate = false;
                }
            }
            else
            {
                break;
            }
            LEFT--;
        }
        int RIGHT = x + 1;
        while (RIGHT < Xmax)
        {
            if (temp == objArray[RIGHT, y].GetComponent<objIndex>().index && temp < 10)
            {
                h++;
                if (objArrayIndex[RIGHT, y] == 100 || objArrayIndex[RIGHT, y] == 101 || objArrayIndex[RIGHT, y] == 102)
                {
                    horiInstantiate = false;
                }
                if (objArrayIndex[RIGHT, y] == 100)
                {
                    crossinstantiate = false;
                }
            }
            else
            {
                break;
            }
            RIGHT++;
        }
    }

    void changeLineIndex(int temp, int x, int y)
    {
        objArrayIndex[x, y] = 99;
        sendObjective(temp);
    }

    void changeBothLineIndex(int temp, int x, int y)
    {
        objArrayIndex[x, y] = 100;
        sendObjective(temp);
    }

    void changeLineIndexHS(int temp, int x, int y)
    {
        objArrayIndex[x, y] = 101;
        sendObjective(temp);
    }

    void changeLineIndexVS(int temp, int x, int y)
    {
        objArrayIndex[x, y] = 102;
        sendObjective(temp);
    }

    // set animator parameter in each object being destryed to play dead animation.
    void setAnimate()
    {
        for (int j = 0; j < Ymax; j++)
        {
            for (int i = 0; i < Xmax; i++)
            {
                if (objArrayIndex[i, j] >= 99)
                {
                    Vector3 inputPos = locations[i, j].GetComponent<Transform>().position;
                    RaycastHit2D hit = Physics2D.Raycast(inputPos, Vector2.zero);
                    if (hit)
                    {
                        print("hit");
                        if (hit.collider.tag == "floor")
                        {
                            print("floor");
                            Destroy(hit.transform.gameObject);
                        }
                    }
                    objArray[i, j].GetComponent<Animator>().SetBool("dead", true);
                    itemCount++;
                    animating = true;
                }
            }
        }
        state = State.Destroy;
    }

    // send elements destoying data to objective collecting script.
    void sendObjective(int temp)
    {
        if (temp == 1)
        {
            GetComponent<objectives>().block1++;
        }
        if (temp == 2)
        {
            GetComponent<objectives>().block2++;
        }
        if (temp == 3)
        {
            GetComponent<objectives>().block3++;
        }
        if (temp == 4)
        {
            GetComponent<objectives>().block4++;
        }
    }

    // move objects to null space below
    void moveObjects()
    {
        animating = true;
        for (int x = 0; x < Xmax; x++)
        {
            for (int y = Ymax-1; y > 0; y--)
            {
                locations[x, y].GetComponent<moveObjs>().enabled = true;
                if (objArray[x,y] == null)
                {
                    for (int i = y-1; i > -1; i--)
                    {
                        if (objArray[x,i] != null)
                        {
                            locations[x, y].GetComponent<moveObjs>().sendLerp(locations[x,i].GetComponent<Transform>().position, locations[x,y].GetComponent<Transform>().position, objArray[x,i]);
                            objArrayIndex[x, y] = objArrayIndex[x, i];
                            objArrayIndex[x, i] = 99;
                            objArray[x, y] = objArray[x, i];
                            objArray[x, y].GetComponent<objIndex>().y += y-i;
                            objArray[x, i] = null;
                            break;
                        }
                    }
                }
            }
        }
        if (Time.time > moveExit)
        {
            state = State.CreateNormal;
        }
    }

    // calculate multiplier and set multiplier text to appear/disappear
    void multiplierCal()
    {
        if (itemCount > 5)
        {
            multiplier = 2;
            if (itemCount > 10)
            {
                multiplier = 4;
                if (itemCount > 15)
                {
                    multiplier = 8;
                    if (itemCount > 20)
                    {
                        multiplier = 16;
                        if (itemCount > 50)
                        {
                            multiplier = 32;
                            if (itemCount > 75)
                            {
                                multiplier = 64;
                            }
                        }
                    }
                }
            }
        }
        // set multiplier text to appear/disappear
        if (multiplier >= 2)
        {
            bonustxt.color = Color.white;
            bonustxt.text = "x " + multiplier;
        }
        else
        {
            bonustxt.color = Color.clear;
        }
    }
}
