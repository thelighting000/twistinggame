﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OnMouseOverCenter : MonoBehaviour {
    Color m_MouseOverColor = Color.yellow;
    Color m_OriginalColor;
    SpriteRenderer s_Renderer;

    GameObject togg;
    Toggle m_Toggle;

    public GameObject circle, circleReverse;
    private GameObject tempObj;
    private Vector2 objCenter;

    void Start()
    {
        s_Renderer = GetComponent<SpriteRenderer>();
        m_OriginalColor = s_Renderer.color;
        objCenter = GetComponent<Transform>().position;

        togg = GameObject.Find("Toggle");
        m_Toggle = togg.GetComponent<Toggle>();
    }

    void OnMouseEnter()
    {
        if (m_Toggle.isOn)
        {
            s_Renderer.material.color = m_MouseOverColor;
            tempObj = Instantiate(circle, objCenter, Quaternion.identity);
        }
        else if (!m_Toggle.isOn)
        {
            s_Renderer.material.color = m_MouseOverColor;
            tempObj = Instantiate(circleReverse, objCenter, Quaternion.identity);
        }
    }

    void OnMouseExit()
    {
        s_Renderer.material.color = m_OriginalColor;
        Destroy(tempObj);
    }
}
