﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class aiScript : MonoBehaviour {
    public float hunger, thirst, love;
    public float max_hunger, max_thirst, max_love;
    float hunger_time, thirst_time, love_time, tick;

    public float speed;
    public float randomX;
    public float randomY;
    public float minWaitTime;
    public float maxWaitTime;
    private Vector3 currentRandomPos;

    bool randomMove, foodMove, waterMove;

    // Use this for initialization
    void Start () {
        hunger = max_hunger;
        thirst = max_thirst;
        love = max_love;
        tick = 0.1f;
        hunger_time = Time.time;
        thirst_time = Time.time;
        love_time = Time.time;

        randomMove = true;
        foodMove = false;
        waterMove = false;
    }

    // Update is called once per frame
    void Update () {
        petNeed();
        limitCheck();

        PickPosition();

    }

    void limitCheck ()
    {
        if (hunger >= max_hunger)
        {
            hunger = max_hunger;
            foodMove = false;
        }
        if (thirst >= max_thirst)
        {
            thirst = max_thirst;
            waterMove = false;
        }
        if (love >= max_love)
        {
            love = max_love;
        }
    }

    void petNeed ()
    {
        if (hunger > 0)
        {
            if (Time.time > hunger_time)
            {
                hunger -= 0.1f;
                hunger_time += tick;
            }
        }
        else
        {
            GetComponent<SpriteRenderer>().color = Color.red;
            foodMove = true;
        }
        if (thirst > 0)
        {
            if (Time.time > thirst_time)
            {
                thirst -= 0.1f;
                thirst_time += tick;
            }
        }
        else
        {
            GetComponent<SpriteRenderer>().color = Color.blue;
        }
        if (love > 0)
        {
            if (Time.time > love_time)
            {
                love -= 0.1f;
                love_time += tick;
            }
        }
        else
        {
            GetComponent<SpriteRenderer>().color = Color.gray;
        }
    }

    void PickPosition()
    {
        if (randomMove && (!foodMove || !waterMove))
        {
            currentRandomPos = new Vector3(Random.Range(-randomX, randomX), Random.Range(-randomY, randomY), 0);
            StartCoroutine(MoveToRandomPos());
            randomMove = false;
        }
    }

    IEnumerator MoveToRandomPos()
    {
        float i = 0.0f;
        float rate = 1.0f / speed;
        Vector3 currentPos = transform.position;

        while (i < 1.0f)
        {
            i += Time.deltaTime * rate;
            transform.position = Vector3.Lerp(currentPos, currentRandomPos, i);
            yield return null;
        }

        float randomFloat = Random.Range(0.0f, 1.0f); // Create %50 chance to wait
        if (randomFloat < 0.5f)
            StartCoroutine(WaitForSomeTime());
        else
        {
            randomMove = true;
            PickPosition();
        }
    }

    IEnumerator WaitForSomeTime()
    {
        yield return new WaitForSeconds(Random.Range(minWaitTime, maxWaitTime));
        randomMove = true;
        PickPosition();
    }
}
