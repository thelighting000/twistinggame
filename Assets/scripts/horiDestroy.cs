﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class horiDestroy : MonoBehaviour {
    int x, y;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        x = (int)GetComponent<objIndex>().arrayindex.x;
        y = (int)GetComponent<objIndex>().arrayindex.y;
        if (GetComponent<objIndex>().horiDead == true)
        {
            GameObject getscript = GameObject.Find("scriptbox");
            getscript.GetComponent<Elements>().horiClick(x, y);
        }
    }
}
