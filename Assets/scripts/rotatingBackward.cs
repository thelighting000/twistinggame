﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class rotatingBackward : MonoBehaviour
{
    private Rigidbody2D rb2D;
    private float revSpeed = -50.0f;

    // Use this for initialization
    void Start()
    {
        rb2D = gameObject.AddComponent<Rigidbody2D>();
        rb2D.gravityScale = 0.0f;
    }

    // Update is called once per frame
    void Update()
    {
        rb2D.MoveRotation(rb2D.rotation - revSpeed * Time.fixedDeltaTime);
    }
}