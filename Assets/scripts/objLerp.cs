﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class objLerp : MonoBehaviour {
    public float speed;
    float startTime;
    Vector2 transA, transB, transC, transD;
    Vector2 A, B, C, D;
    GameObject ObjA, ObjB, ObjC, ObjD;

    // Use this for initialization
    void Start () {

    }

    // Update is called once per frame
    void Update()
    {
        float distCovered = (Time.time - startTime) * speed;
        if (ObjA != null && ObjB != null && ObjC != null && ObjD != null)
        {
            ObjA.transform.position = Vector3.Lerp(transA, B, distCovered / Vector2.Distance(transA, B));
            ObjB.transform.position = Vector3.Lerp(transB, C, distCovered / Vector2.Distance(transB, C));
            ObjC.transform.position = Vector3.Lerp(transC, D, distCovered / Vector2.Distance(transC, D));
            ObjD.transform.position = Vector3.Lerp(transD, A, distCovered / Vector2.Distance(transD, A));
        }
        //print(transA.x+", "+transA.y);
    }

    public void sendTransA(Vector2 A, Vector2 B, GameObject obja)
    {
        transA = A;
        this.B = B;
        ObjA = obja;
        startTime = Time.time;
    }

    public void sendTransB(Vector2 B, Vector2 C, GameObject objb)
    {
        transB = B;
        this.C = C;
        ObjB = objb;
    }

    public void sendTransC(Vector2 C, Vector2 D, GameObject objc)
    {
        transC = C;
        this.D = D;
        ObjC = objc;
    }

    public void sendTransD(Vector2 D, Vector2 A, GameObject objd)
    {
        transD = D;
        this.A = A;
        ObjD = objd;
    }
}
