﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class vertiDestroy : MonoBehaviour {
    int x, y;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        x = (int)GetComponent<objIndex>().arrayindex.x;
        y = (int)GetComponent<objIndex>().arrayindex.y;
        if (GetComponent<objIndex>().vertiDead == true)
        {
            GameObject getscript = GameObject.Find("scriptbox");
            getscript.GetComponent<Elements>().vertiClick(x, y);
        }
    }
}
