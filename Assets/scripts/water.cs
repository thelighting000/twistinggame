﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class water : MonoBehaviour {
    public float watervalue;
    public float max_water;
    // Use this for initialization
    void Start () {
        watervalue = 0;
	}
	
	// Update is called once per frame
	void Update () {
        if (watervalue >= max_water)
        {
            watervalue = max_water;
        }
        if (watervalue >= max_water*75/100)
        {
            GetComponent<SpriteRenderer>().color = Color.green;
        }
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "pet")
        {
            if (watervalue > 0)
            {
                collision.GetComponent<aiScript>().thirst += 10;
                watervalue -= 10;
            }
        }
    }
}
