﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MouseClick : MonoBehaviour {
    // Use this for initialization
    GameObject clickedobj, togg;
    Toggle m_Toggle;
    int hitx, hity;

    float nextClick;
    public float clickRate;

    void Start () {
        nextClick = Time.time;

        togg = GameObject.Find("Toggle");
        m_Toggle = togg.GetComponent<Toggle>();
    }
	
	// Update is called once per frame
	void Update () {
        Vector3 mousePos = Input.mousePosition;
        mousePos.z = 10;
        Vector3 screenPos = Camera.main.ScreenToWorldPoint(mousePos);
        RaycastHit2D hit = Physics2D.Raycast(screenPos, Vector2.zero);
        if (Input.GetMouseButtonDown(0) && Time.time > nextClick)
        {
            nextClick = Time.time + clickRate;
            if (hit && GetComponent<Elements>().animating == false)
            {
                GetComponent<Elements>().state = Elements.State.Click;
                if (hit.collider.tag == "spiningcenter" && m_Toggle.isOn && !hit.transform.gameObject.GetComponent<spinnerLock>().isLocked)
                {
                    hitx = hit.collider.GetComponent<objIndex>().x;
                    hity = hit.collider.GetComponent<objIndex>().y;
                    GetComponent<Elements>().updatePosition(hitx, hity);
                    GetComponent<Elements>().objCheck(hitx, hity);
                    if (GetComponent<gameManager>().move == true)
                    {
                        GetComponent<gameManager>().moves--;
                    }
                }
                if (hit.collider.tag == "spiningcenter" && !m_Toggle.isOn && !hit.transform.gameObject.GetComponent<spinnerLock>().isLocked)
                {
                    hitx = hit.collider.GetComponent<objIndex>().x;
                    hity = hit.collider.GetComponent<objIndex>().y;
                    GetComponent<Elements>().reversePosition(hitx, hity);
                    GetComponent<Elements>().objCheck(hitx, hity);
                    if (GetComponent<gameManager>().move == true)
                    {
                        GetComponent<gameManager>().moves--;
                    }
                }
                if (hit.collider.tag == "specialelem")
                {
                    hit.collider.GetComponent<objIndex>().speDead = true;
                }
                if (hit.collider.tag == "hori")
                {
                    hit.collider.GetComponent<objIndex>().horiDead = true;
                }
                if (hit.collider.tag == "verti")
                {
                    hit.collider.GetComponent<objIndex>().vertiDead = true;
                }
            }
        }
    }
}
