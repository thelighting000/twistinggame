﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class gameManager : MonoBehaviour {
    public int scores, heart, star, grass, flower, moves;
    public float times;
    public bool move, time, floor;
    public Text movetxt, timetxt, hearttxt, startxt, grasstxt, block4txt, scoretxt, resulttxt;
    public Slider timeslider;
    public GameObject floorPref;
    public int floorXmax, floorYmax;

    bool complete, gameover, score, heart_bool, star_bool, grass_bool, flower_bool;
    float dangertime;

	// Use this for initialization
	void Start () {
        complete = false;
        gameover = false;
        score = false;
        dangertime = times * 20 / 100;

        if (time)
        {
            timeslider.maxValue = times;
        }

        if (floor)
        {
            for (int y = 0; y < floorYmax; y++)
            {
                for (int x = 0; x < floorXmax; x++)
                {
                    GameObject tempObj = Instantiate(floorPref, new Vector2(x - floorXmax / 2, y - floorYmax / 2), Quaternion.identity);
                }
            }
        }

        if (heart == 0)
        {
            heart_bool = true;
        }
        else heart_bool = false;
        if (star == 0)
        {
            star_bool = true;
        }
        else star_bool = false;
        if (grass == 0)
        {
            grass_bool = true;
        }
        else grass_bool = false;
        if (flower == 0)
        {
            flower_bool = true;
        }
        else flower_bool = false;
	}
	
	// Update is called once per frame
	void Update () {
        int getscore = GetComponent<Elements>().score;
        int getheart = GetComponent<objectives>().block1;
        int getblock2 = GetComponent<objectives>().block2;
        int getblock3 = GetComponent<objectives>().block3;
        int getblock4 = GetComponent<objectives>().block4;

        hearttxt.text = getheart + "/" + heart;
        startxt.text = getblock2 + "/" + star;
        grasstxt.text = getblock3 + "/" + grass;
        block4txt.text = getblock4 + "/" + flower;
        if (move)
        {
            movetxt.color = Color.white;
            movetxt.text = "moves : " + moves;
        }

        if (time)
        {
            timeslider.value = times;
            timetxt.color = Color.white;
            timetxt.GetComponent<UnityEngine.UI.Text>().text = "time : ";
            if (timeslider.value <= dangertime)
            {
                GameObject fill = GameObject.Find("Fill");
                fill.GetComponent<Image>().color = Color.red;
            }
        }

        scoretxt.text = getscore + "/" + scores;

        if (score == true && heart_bool == true && star_bool == true && grass_bool == true && flower_bool == true)
        {
            complete = true;
            Debug.Log("complete!");
            resulttxt.color = Color.white;
            resulttxt.text = "COMPLETE!";
        }

        if (move)
        {
            if (moves > 0)
            {
                if (scores > 0 && getscore >= scores)
                {
                    score = true;
                }
                if (heart > 0 && getheart >= heart)
                {
                    heart_bool = true;
                }
                if (star > 0 && getblock2 >= star)
                {
                    star_bool = true;
                }
                if (grass > 0 && getblock3 >= grass)
                {
                    grass_bool = true;
                }
                if (flower > 0 && getblock4 >= flower)
                {
                    flower_bool = true;
                }
            }
            else
            {
                gameover = true;
                Debug.Log("gameover");
                resulttxt.color = Color.white;
                resulttxt.text = "GAMEOVER!";
            }
        }

        if (time)
        {
            if (times > 0 && !complete)
            {
                times -= Time.deltaTime;
            }
            int min = Mathf.FloorToInt(times / 60);
            int sec = Mathf.FloorToInt(times % 60);
            timetxt.GetComponent<UnityEngine.UI.Text>().text = "time : " + min.ToString("00") + ":" + sec.ToString("00");
            if (times > 0)
            {
                if (scores > 0 && getscore >= scores)
                {
                    score = true;
                }
                if (heart > 0 && getheart >= heart)
                {
                    heart_bool = true;
                }
                if (star > 0 && getblock2 >= star)
                {
                    star_bool = true;
                }
                if (grass > 0 && getblock3 >= grass)
                {
                    grass_bool = true;
                }
                if (flower > 0 && getblock4 >= flower)
                {
                    flower_bool = true;
                }
            }
            else
            {
                gameover = true;
                Debug.Log("gameover");
                resulttxt.color = Color.white;
                resulttxt.text = "GAMEOVER!";
            }
        }
	}
}
