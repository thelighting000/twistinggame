﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class moveObjs : MonoBehaviour {
    public float speed;
    [SerializeField]
    float startTime;
    Vector2 transA;
    Vector2 A, B;
    GameObject ObjA;

    // Use this for initialization
    void Start () {
		
	}

    private void Update()
    {
        float distCovered = (Time.time - startTime) * speed;
        if (ObjA != null)
        {
            ObjA.transform.position = Vector3.Lerp(transA, B, distCovered / Vector2.Distance(transA, B));
        }
    }

    public void sendLerp(Vector2 A, Vector2 B, GameObject obja)
    {
        transA = A;
        this.B = B;
        ObjA = obja;
        startTime = Time.time;
    }
}
