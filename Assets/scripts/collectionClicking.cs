﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class collectionClicking : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}

    // Update is called once per frame
    void Update()
    {
        Vector3 mousePos = Input.mousePosition;
        mousePos.z = 10;
        Vector3 screenPos = Camera.main.ScreenToWorldPoint(mousePos);
        RaycastHit2D hit = Physics2D.Raycast(screenPos, Vector2.zero);
        if (Input.GetMouseButtonDown(0))
        {
            if (hit)
            {
                if (hit.collider.tag == "pet")
                {
                    hit.collider.GetComponent<aiScript>().love += 10;
                    hit.collider.GetComponent<SpriteRenderer>().color = Color.yellow;
                    print("hit");
                }
                if (Input.GetMouseButton(0))
                {
                    if (hit.collider.tag == "food")
                    {
                        hit.collider.GetComponent<food>().foodvalue += 10;
                    }
                }
                if (hit.collider.tag == "water")
                {
                    hit.collider.GetComponent<water>().watervalue += 15;
                }
            }
        }
        if (Input.GetMouseButton(0))
        {
            if (hit.collider.tag == "food" || hit.collider.tag == "pet" || hit.collider.tag == "water")
            {
                hit.collider.GetComponent<Transform>().position = screenPos;
            }
        }
    }
}
