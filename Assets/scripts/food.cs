﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class food : MonoBehaviour {
    public float foodvalue;
    public float max_food;
	// Use this for initialization
	void Start () {
        foodvalue = 0;
	}
	
	// Update is called once per frame
	void Update () {
        if (foodvalue >= max_food)
        {
            foodvalue = max_food;
        }
        if (foodvalue >= max_food*75/100)
        {
            GetComponent<SpriteRenderer>().color = Color.red;
        }
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "pet")
        {
            if (foodvalue > 0)
            {
                collision.GetComponent<aiScript>().hunger += 10;
                foodvalue -= 10;
            }
        }
    }
}
